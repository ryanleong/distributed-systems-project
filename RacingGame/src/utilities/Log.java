package utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class Log {

	/* Indicates whether to print to console */
	private boolean VERBOSE = true;

	private String logFileName = "log.txt";

	public Log() {

	}

	public void print(String tag, String data) {
		Date current = new Date();
		String outputString = current.toString() + "\t|| " + tag + ":: " + data + "\n";

		if (VERBOSE) {
			System.out.print(outputString);
		}

		writeLog(outputString);
	}

	public void println(String tag, String data) {
		Date current = new Date();
		String outputString = current.toString() + "\t|| " + tag + ":: " + data + "\n";

		if (VERBOSE) {
			System.out.println(outputString);
		}

		writeLog(outputString);
	}

	private void writeLog(String outputString) {
		BufferedWriter bufferWritter = null;
		
		try {
			File file = new File(logFileName);

			// Create file if it does not exist
			if (!file.exists()) {
				file.createNewFile();
			}

			// true = append file
			FileWriter fileWritter = new FileWriter(file.getName(), true);

			bufferWritter = new BufferedWriter(fileWritter);
			bufferWritter.write(outputString);
			

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bufferWritter.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean isDebugOn() {
		return VERBOSE;
	}

	public void enableDebug() {
		VERBOSE = true;
	}

	public void disableDebug() {
		VERBOSE = false;
	}

	public String getLogFileName() {
		return logFileName;
	}

	public void setLogFileName(String logFileName) {
		this.logFileName = logFileName;
	}

}
