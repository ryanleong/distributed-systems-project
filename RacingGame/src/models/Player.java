package models;

import java.net.InetAddress;
import java.util.ArrayList;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

import controller.Game;

public class Player { // initiate the player class

	private Image car;

	private double x, y, prevX = x, prevY = y; /*
												 * gives values of x,y to a temp
												 * previous this is required as
												 * it helps when a crash
												 * happens, allowing a quick
												 * revert
												 */

	private double velocity = 0.0, // initializing velocity
			velMulti = 0.0005; // velocity multiplier, changes game speed

	// Address and Port of Client System
	private InetAddress ipAddress;
	private int port;

	public Player(String imageLocation, double x, double y)
			throws SlickException {
		car = new Image(imageLocation);

		this.x = x;
		this.y = y;
	}

	public void render(Graphics g, int cam_x, int cam_y, double x, double y,
			Angle theta) {

		// Calculate the kart's on-screen location from the camera
		int screen_x = (int) (x - cam_x);
		int screen_y = (int) (y - cam_y);

		car.setRotation((float) theta.getDegrees());
		car.drawCentered(screen_x, screen_y);

	}

	/*
	 * The two following statements below make life easier by returning the half
	 * of the screen, used in map.render in World.
	 */

	public int tileAlignX() {
		return (Game.SCREENWIDTH / 2);
	}

	public int tileAlignY() {
		return (Game.SCREENHEIGHT / 2);
	}

	public Double[] update(ArrayList<Player> players, double moveDir,
			int delta, Angle theta, int playerID, int id, double mu2, TiledMap map) {

		// The section following is to gain velocity

		double x1 = players.get(playerID).getX(), y1 = players.get(playerID)
				.getY();

		// the formula for velocity, .pow is very useful here
		velocity = (velocity + velMulti * moveDir * delta)
				* (Math.pow(1 - mu2, delta));

		/*
		 * These four lines enable the kart to move It first updates x1 and y1,
		 * and then feeds it to setX, setY to make the kart move around. Each
		 * formula gets the polar coordinates in order to move in each direction
		 */
		x1 = x1 + theta.getXComponent(velocity * delta);
		y1 = y1 + theta.getYComponent(velocity * delta);
		int id1 = map.getTileId(
				(int) x1 / map.getTileWidth(),
				(int) y1 / map.getTileHeight(), 0);
		
		String mew = map.getTileProperty(id1, "friction", null);
		
		
		double mu = Double.parseDouble(mew);
		if(mu!=1){
		players.get(playerID).setX(x1);
		players.get(playerID).setY(y1);
		}
		
		/*
		 * This interprets the id of the tile the kart point is currently
		 * sitting on. This will aid in determining the coefficient of friction,
		 * mu
		 */
		int i = 0;
		for (Player player : players) {
			
			if (i != Game.myID){
				if (detectCollision(Game.playerCoords.get(i)[0], Game.playerCoords.get(i)[1],
						players.get(playerID))) {
					velocity = 0;
					players.get(playerID)
							.setX(players.get(playerID).getPrevX());
					// System.out.println(players.get(playerID).getX());
					players.get(playerID)
							.setY(players.get(playerID).getPrevY());
					// System.out.println(players.get(playerID).getY());}
				}
				
			}
			
			i++;
		}

			/*
			 * Below, we find the coefficient of friction by matching the id to
			 * the getTileProperty. We need to Double.parseDouble it to get a
			 * decimal number between 0 and 1, not 0 or 1, which would occur
			 * (integer) and would not be too useful.
			 */
			// mu = Double.parseDouble(map.getTileProperty(id, "friction",
			// null));

			/*
			 * The if statement below provides a fail safe in the event we hit a
			 * wall or an object with mu==1. It immediately sets the velocity to
			 * 0 and then reverses the kart to the position it was in before it
			 * hit as to avoid the kart getting stuck. If mu!=1, then we set the
			 * previous coordinates as the new fail safe. This ensures we will
			 * never get stuck
			 */
			
			if (mu2 == 1) {
				velocity = 0;
//				players.get(playerID).setY(players.get(playerID).getPrevY());
//				players.get(playerID).setX(players.get(playerID).getPrevX());
				// System.out.println(players.get(playerID).getX());
				// System.out.println(players.get(playerID).getY());
				if (players.get(playerID).getPrevY() < players.get(playerID)
						.getY()) {
					players.get(playerID)
					.setY(players.get(playerID)
							.getY() - theta.getYComponent(20));
					
				} else {
					players.get(playerID)
					.setY(players.get(playerID)
							.getY() + theta.getYComponent(20));
					
					// System.out.println(players.get(playerID).getX());
				}
				if (players.get(playerID).getPrevX() > players.get(playerID).getX()) {
					players.get(playerID)
							.setX(getX() + theta.getXComponent(20));

				} else {
					players.get(playerID)
							.setX(getX() - theta.getXComponent(20));
					// System.out.println(players.get(playerID).getX());
				}
			} else {
				if (players.get(playerID).getPrevY() < players.get(playerID).getY()) {
					players.get(playerID)
					.setPrevY(players.get(playerID)
							.getY() + theta.getYComponent(20));
					
				} else {
					players.get(playerID)
					.setPrevY(players.get(playerID)
							.getY() - theta.getYComponent(20));
					
					// System.out.println(players.get(playerID).getX());
				}
				if (players.get(playerID).getPrevX() > players.get(playerID)
						.getX()) {
					players.get(playerID)
							.setPrevX(getX() - theta.getXComponent(20));

				} else {
					players.get(playerID)
							.setPrevX(getX()+ theta.getXComponent(20));
					// System.out.println(players.get(playerID).getX());
				}

			}
		
		if (players.get(playerID).getY() <= 3500)
			velocity = 0;

		return new Double[] { players.get(playerID).getX(),
				players.get(playerID).getY(), theta.getRadians()};

	}

	/*
	 * The below eight statements created getters and setters as the variables
	 * they are are private. This allows them to be accessible in the World
	 */

	public double getPrevX() {
		return prevX;
	}

	public void setPrevX(double prevX) {
		this.prevX = prevX;
	}

	public double getPrevY() {
		return prevY;
	}

	public void setPrevY(double prevY) {
		this.prevY = prevY;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public InetAddress getIpAddress() {
		return ipAddress;
	}

	public void setIpAddress(InetAddress ipAddress) {
		this.ipAddress = ipAddress;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public boolean detectCollision(double x, double y, Player player) {
		double xDiff, yDiff;

		xDiff = Math.pow(x - player.getX(), 2);
		yDiff = Math.pow(y - player.getY(), 2);
		return ((Math.sqrt(xDiff + yDiff) < 40));

	}
}
