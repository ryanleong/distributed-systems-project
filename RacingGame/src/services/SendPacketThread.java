package services;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;

import controller.Game;

public class SendPacketThread implements Runnable {

	public SendPacketThread() {

	}

	@Override
	public void run() {

		while (true) {

			if (Game.myID == 0) {
				runAsServer();
			} else {
				runAsClient();
			}

			// Sleep
			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
				Game.log.println("Sending Thread",
						"Unable to pause thread for delayed sending.");
			}
		}
	}

	private void runAsServer() {
		/** FORMAT OF DATA TO BE SENT */
		// ID,x,y|ID,x,y|ID,x,y
		String packetData = "";

		// Create data to be sent to clients
		for (int i = 0; i < Game.players.size(); i++) {
			packetData = packetData + i + ",";
			packetData = packetData + Game.players.get(i).getX() + ",";
			packetData = packetData + Game.players.get(i).getY() + ",";
			packetData = packetData + Game.playerCoords.get(i)[2] + "|";
		}

		try {
			byte[] packetByteData = packetData.getBytes();

			// Send to all clients
			for (int i = 1; i < Game.players.size(); i++) {

				DatagramPacket sendPacket = new DatagramPacket(packetByteData,
						packetByteData.length, Game.players.get(i)
								.getIpAddress(), Game.players.get(i).getPort());

				Game.socket.send(sendPacket);
			}
		} catch (IOException e) {
			Game.log.println("Send as server",
					"Unable to send packet to client.");
		}
	}

	private void runAsClient() {
		/** FORMAT OF DATA TO BE SENT */
		// ID,x,y
		String packetData = "";

		packetData = packetData + Game.myID + ",";
		packetData = packetData + Game.players.get(Game.myID).getX() + ",";
		packetData = packetData + Game.players.get(Game.myID).getY() + ",";
		packetData = packetData + Game.playerCoords.get(Game.myID)[2];

		try {
			byte[] packetByteData = packetData.getBytes();

			DatagramPacket sendPacket = new DatagramPacket(packetByteData,
					packetByteData.length,
					InetAddress.getByName(Game.serverIP), Game.serverPort);

			Game.socket.send(sendPacket);

		} catch (IOException e) {
			Game.log.println("Send as client",
					"Unable to send packet to server.");
		}
	}
}
