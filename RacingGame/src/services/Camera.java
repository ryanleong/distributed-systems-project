package services;

import models.Angle;
import models.Player;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

public class Camera {
	private int gridX = 24, gridY = 18; 	// the grid (x,y) to be rendered at a time
	
	public void render(Angle theta,Player player, TiledMap map) 
			throws SlickException {
		map.render(
				(((int) (player.tileAlignX() - player.getX()) % map.getTileWidth())),
				(((int) (player.tileAlignY() - player.getY()) % map.getTileHeight())),
				(((int) (player.getX() - player.tileAlignX()) / map.getTileWidth())),
				(((int) (player.getY() - player.tileAlignY()) / map.getTileHeight())),
				gridX, gridY);


	
	}

}
