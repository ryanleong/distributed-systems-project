package services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import models.Angle;
import models.Player;

import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.tiled.TiledMap;

import controller.Game;

/**
 * Represents the entire game world. (Designed to be instantiated just once for
 * the whole game).
 */

public class World { // initiate the world
	private TiledMap map; // the map that will be rendered

	private Camera camera;
	private Panel panel;
	private ArrayList<Angle> theta = new ArrayList<Angle>(); 

	private double velocity = 0.0, // initializing velocity
			velMulti = 0.0005, // velocity multiplier, changes game speed
			mu = 0.0, // initializing friction constant
			rotateSpd = 0.004; // rotation speed

//	private int winners =0;
	

	/**
	 * Create a new World object.
	 * 
	 * @param 
	 */
	public World() throws SlickException {
		// creates the objects of the world, including the player
		map = new TiledMap("assets/map3.tmx", "assets/");
		
		
		for (int i = 0; i < Game.players.size() ;i++)
		   theta.add(new Angle(0.0));

		camera = new Camera();
		panel = new Panel();
	}

	/**
	 * Update the game state for a frame.
	 * 
	 * @param rotate_dir
	 *            The player's direction of rotation (-1 for anti-clockwise, 1
	 *            for clockwise, or 0).
	 * @param move_dir
	 *            The player's movement in the car's axis (-1, 0 or 1).
	 * @param delta
	 *            Time passed since last frame (milliseconds).
	 * @param playerCoords 
	 * @return 
	 */

	public HashMap<Integer, Double[]> update(double rotate_dir, double move_dir, int delta, 
			ArrayList<Player> players, HashMap<Integer, Double[]> playerCoords)
			throws SlickException {
		

		for (int i = 0; i < players.size(); i++) {

		/*
		 * This is to obtain the current angle and to update the angle the car
		 * is at.
		 */
			if(i == Game.myID){
			Angle prevTheta = theta.get(i);
			Angle tmpAngle = new Angle(rotateSpd * rotate_dir * delta);
		    theta.set(i, prevTheta.add(tmpAngle));	

			int id = map.getTileId(
					(int) players.get(i).getX() / map.getTileWidth(),
					(int) players.get(i).getY() / map.getTileHeight(), 0);
			
			String mew = map.getTileProperty(id, "friction", null);
			
			
			mu = Double.parseDouble(mew);

			if(players.get(i).getY() <= 3500)
				move_dir = 0;
			
		Double[] arr = players.get(i).update( players, move_dir, delta, theta.get(i),
				i, id, mu,  map);
		System.out.println("Array returned " + arr[0] + " " + arr[1] + " " + arr[2]);
		Game.playerCoords.put(i, arr);	
		 }

		}

		return Game.playerCoords;


	}

	/**
	 * Render the entire screen, so it reflects the current game state.
	 * 
	 * @param g
	 *            The Slick graphics object, used for drawing.
	 * @param playerCoords 
	 */
	/*public void render(Graphics g) throws SlickException {

		/*
		 * Render the map This requires filling the parameters of map render as
		 * follows (x,y) the coordinates of the pixels (sx, sy) the coordinates
		 * of the tiles (width, height) the coordinates to render at any time
		 * The first two coordinates come out as negative, which aligns the map
		 * out properly. We need both pixels and tiles to properly render the
		 * map otherwise errors will be encountered.
		 * 
		 * The process to find each coordinate is intricate, with the
		 * relationships using the half screen width(height) to do it's final
		 * calculations. Each rendering frame calculates the position of the map
		 * as per p1 from update.
		 * 
		 * Converting to integers is required as these numbers should be whole
		 */
	
	public void render(Graphics g,  ArrayList<Player> players, HashMap<Integer, Double[]> playerCoords) throws SlickException {
		
		// Calculate the top-left corner of the screen, in pixels, relative to
		// the top-left corner of the map -- center the camera around the
		// player
		int cam_x = (int) players.get(Game.myID).getX() - (Game.SCREENWIDTH / 2);
		int cam_y = (int) players.get(Game.myID).getY() - (Game.SCREENHEIGHT / 2);
		
		// Calculate the camera location (in tiles) and offset (in pixels)
		int cam_tile_x = cam_x / this.map.getTileWidth();
		int cam_tile_y = cam_y / this.map.getTileHeight();
		int cam_offset_x = cam_x % this.map.getTileWidth();
		int cam_offset_y = cam_y % this.map.getTileHeight();

		// Render 24x18 tiles of the map to the screen, starting from the
		// camera location in tiles (rounded down). Begin drawing at a
		// negative offset relative to the screen, to ensure smooth scrolling.
		int screen_tilew = Game.SCREENWIDTH / this.map.getTileWidth() + 2;
		int screen_tileh = Game.SCREENHEIGHT / this.map.getTileHeight() + 2;
		this.map.render(-cam_offset_x, -cam_offset_y, cam_tile_x, cam_tile_y,
				screen_tilew, screen_tileh);
        
		// Render the player
		for (int i = 0; i < players.size(); i++){
			if(i == Game.myID)
				{
				players.get(i).render(g, cam_x, cam_y, playerCoords.get(Game.myID)[0],playerCoords.get(Game.myID)[1], theta.get(Game.myID));
				//System.out.println("Location" +  playerCoords.get(Game.myID)[0] + " " +  playerCoords.get(Game.myID)[1]);
				}
			else
				{
				System.out.println("Location" +  playerCoords.get(i)[0] + " " +  playerCoords.get(i)[1]);
				players.get(i).render(g, cam_x, cam_y, playerCoords.get(i)[0],playerCoords.get(i)[1],new Angle(playerCoords.get(i)[2])) ;
				}
				
//		players.get(1).render(g, cam_x, cam_y, theta.get(1));
		       // player.render(g, cam_x, cam_y, theta.get(Game.myID));
		}
		
		panel.render(g, getRank(playerCoords));
		if(playerCoords.get(Game.myID)[1] <= 3500){
           // winners++;
			g.drawString("You came " + Panel.ordinal(getRank(playerCoords)), Game.SCREENWIDTH/2 - 55, Game.SCREENHEIGHT/2 - 60);
		}
		
	}

	private int getRank(HashMap<Integer, Double[]> playerCoords) {
		Double[] arr  = new Double[playerCoords.size()];
		int winners = 0;
		
        for(Map.Entry<Integer, Double[]> en: playerCoords.entrySet()){
        	    	arr[en.getKey()] = en.getValue()[1];
//        	    	if(arr[en.getKey()] <= 3600){
//        	    		winners++;
//        	    		arr[en.getKey()]= (double) 13000;
//        	    	}
        }
      
		Arrays.sort(arr);
//		for (int i = 0; i < arr.length;i++ ){
//			System.out.println("Rank of " + i + " " +  arr[i]);
//					
//		}
		
		return search(arr,playerCoords.get(Game.myID)[1], winners);
	}
	private int search(Double[] ar, Double arr,int winners){
		for (int i = 0; i < ar.length; i++ ){
			if(ar[i] == arr)
				return winners + i + 1;
		}
		return 1;
	}
		
	
}
