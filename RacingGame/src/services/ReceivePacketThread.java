package services;

import java.io.IOException;
import java.net.DatagramPacket;

import controller.Game;

public class ReceivePacketThread implements Runnable {

	public ReceivePacketThread() {

	}

	@Override
	public void run() {
		byte[] receiveData = new byte[1024];

		while (true) {

			try {
				DatagramPacket receivePacket = new DatagramPacket(receiveData,
						receiveData.length);

				Game.socket.receive(receivePacket);

				String receivedData = new String(receivePacket.getData(),
						receivePacket.getOffset(), receivePacket.getLength(),
						"UTF-8");

				if (Game.myID == 0) {
					runAsServer(receivedData);
				} else {
					runAsClient(receivedData);
				}

			} catch (IOException e) {
				Game.log.println("Receiving Thread",
						"Unable to pause thread for delayed sending.");
			}

		}
	}

	private void runAsServer(String receivedData) {
		String[] userData = receivedData.split(",");
		Double[] coords = { Double.parseDouble(userData[1]),
				Double.parseDouble(userData[2]), Double.parseDouble(userData[3]) };

		Game.playerCoords.put(Integer.parseInt(userData[0]), coords);
	}

	private void runAsClient(String receivedData) {

		String[] allUserData = receivedData.split("\\|");

		for (String userDataString : allUserData) {

			String[] userData = userDataString.split(",");
			Double[] coords = { Double.parseDouble(userData[1]),
					Double.parseDouble(userData[2]), Double.parseDouble(userData[3]) };

			Game.playerCoords.put(Integer.parseInt(userData[0]), coords);
		}
	}
}
