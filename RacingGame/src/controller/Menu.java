package controller;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import com.battlebardgames.tinysui.ActionListener;
import com.battlebardgames.tinysui.Button;
import com.battlebardgames.tinysui.Label;
import com.battlebardgames.tinysui.TextBox;
import com.battlebardgames.tinysui.UiElement;
import com.battlebardgames.tinysui.container.RootUiElement;

public class Menu extends BasicGame {
	private RootUiElement menuItem;
	private boolean isMenu = true;
	
	public Menu() {
		super("UI Demo");
	}
	
	

	@Override
	public void init(GameContainer gc) throws SlickException {
		menuItem = new RootUiElement(gc);
		
    	final TextBox host = new TextBox(400 - 64, 278, 128, 24);
		host.setText("localhost");
		host.setEnabled(true);
		menuItem.addChild(host);
		
		Label pingInfo = new Label(290, 250, 128, 24);
		pingInfo.setText("Enter Server IP Address:");
		menuItem.addChild(pingInfo);
		
		final TextBox port = new TextBox(400 - 64, 298, 128, 24);
		port.setText("9000");
		port.setEnabled(true);
		menuItem.addChild(port);
		
		pingInfo.setText("Enter Port:");
		menuItem.addChild(pingInfo);
		
		final Button serverbtn = new Button(400 - 64, 350, 128, 24);
		serverbtn.setText("Server");
		serverbtn.setClickListener(new ActionListener() {
            @Override
            public void handleEvent(UiElement source) {
            	//Game.setServerIP(host.getText());
            	Game.setClientPort(port.getText());
            	
            	Game.setChoice(1);
            	isMenu = false;
            	
            	try {
					Game.initialize();
				} catch (SlickException e) {
					Game.log.println("Server selection", e.toString());
				}
            	
            }
        });
		
		final Button clientbtn = new Button(400 - 64, 378, 128, 24);
		clientbtn.setText("Client");
		clientbtn.setClickListener(new ActionListener() {
            @Override
            public void handleEvent(UiElement source) {
            	
            	Game.setServerIP(host.getText());
            	Game.setClientPort(port.getText());
            	
            	Game.setChoice(2);
            	isMenu = false;
            	
            	try {
					Game.initialize();
				} catch (SlickException e) {
					Game.log.println("Client selection", e.toString());
				}
            	
            }
        });
		
        menuItem.addChild(serverbtn);
        menuItem.addChild(clientbtn);
        
        Button extbtn = new Button(400 - 64, 406, 128, 24);
        extbtn.setText("Exit");
        
        extbtn.setClickListener(new ActionListener() {
            @Override
            public void handleEvent(UiElement source) {
            	System.exit(0);
            }
        });
        
        
        menuItem.addChild(extbtn);	
	}

	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		if (isMenu) {
			menuItem.update(gc, delta);		
		}
		else {
			Game.updateWorld(gc, delta);
		}
	}

	@Override
	public void render(GameContainer gc, Graphics g) throws SlickException {
		
		if (isMenu) {
			menuItem.render(gc, g);
			g.setBackground(Color.blue);
			g.setColor(Color.white);
			g.drawString("Super Car Racing", 326, 200);
			g.drawString("Distributed Systems Semester 2, 2013", 240, 170);
			g.drawString("University of Melbourne", 300, 150);
			g.drawString("Created By", 360, 500);
			g.drawString("Junee Singh", 351, 520);
			g.drawString("Ryan Leong", 352, 540);
			g.drawString("Zehra Naceem", 345, 560);
			g.drawString("Dean Apostolopoulos", 330, 580);
			
		}
		else {
			Game.renderGame(g);
		}
		
	}
	
	@Override
    public void keyPressed(int key, char c) {
            if(key == Input.KEY_ESCAPE)
                    System.exit(0);
            menuItem.handleInput(key, c);
    }
	
	@Override
    public void mouseMoved(int oldx, int oldy, int newx, int newy) {
            menuItem.mouseMoved(oldx, oldy, newx, newy);
    }
    
    @Override
    public void mouseDragged(int oldx, int oldy, int newx, int newy) {
            menuItem.mouseMoved(oldx, oldy, newx, newy);
    }
    
    @Override
    public void mousePressed(int button, int x, int y) {
            menuItem.mousePressed(button, x, y);
    }
    
    @Override
    public void mouseReleased(int button, int x, int y) {
            menuItem.mouseReleased(button, x, y);
    }
    
    @Override
    public void mouseWheelMoved(int change) {
            menuItem.mouseWheelMoved(change);
    }
	
	public static void main(String [] args){
		try {
			AppGameContainer gc = new AppGameContainer(new Menu(), 800, 600, false);
			gc.start();
		} catch (SlickException e) {
			Game.log.println("Container Creation", e.toString());
		}
	}
	

}