package controller;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import models.Player;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

import services.ReceivePacketThread;
import services.SendPacketThread;
import services.World;
import utilities.Log;


/** Main class for the Car Racing Game engine.
 * Handles initialization, input and rendering.
 */
public class Game extends BasicGame
{
    /** Location of the "assets" directory. */
    public static final String ASSETS_PATH = "assets";

    /** The game state. */
    private static World world;
    
    /** Addresses and Ports*/
    public static String serverIP = "localhost";
    public static int serverPort = 9876;
    public static int clientPort = 5555;
    public static DatagramSocket socket;
    
    public static int myID;
    private static ArrayList<DatagramPacket> playerData;
    public static ArrayList<Player> players = new ArrayList<Player>();
    public static HashMap<Integer, Double[]> playerCoords = new HashMap<Integer, Double[]>();
    public static int choice;
    
    /* For Debugging purposes */
    public static Log log = new Log();
    
    /** Screen width, in pixels. */
    public static final int SCREENWIDTH = 800;
    /** Screen height, in pixels. */
    public static final int SCREENHEIGHT = 600;

   
    
    /** Create a new Game object. */
    public Game() {
    	super("Racing Game");
    }

    public static void setChoice(int choice) {
		Game.choice = choice;
	}
    
    public static void setServerIP(String serverIP) {
  		Game.serverIP = serverIP;
  	}
    
    public static void setServerPort(String serverPort) {
  		Game.serverPort = Integer.parseInt(serverPort);
  	}
    
    public static void setClientPort(String clientPort) {
  		Game.clientPort = Integer.parseInt(clientPort);
  	}
    
	/** Initialize the game state.
     * @param gc The Slick game container object.
     */
    public static void initialize() throws SlickException {
    	int offset = 0;
    	
		switch (choice) {
		case 1:
			// Set to 0 as you are player 1
			myID = 0;
			
			// Set up Socket
			try {
				socket = new DatagramSocket(serverPort);
			} catch (SocketException e) {
				log.println("Server socket creation", e.toString());
				System.exit(1);
			}

			
			// Server to connect client
			Server server = new Server(2);
			playerData = server.waitForClient();
			
			
			// Initialize own player
			Player temp = new Player("assets/cars/car0.png",
					636 + offset, 12018);
			players.add(temp);
			
			// Place data in hashtable
			Double[] coord = {(636.0+offset), 12018.0, 0.0};
			playerCoords.put(0, coord);
			
			offset = offset + 100;
			
			// Initialization of other Player Objects
			for (int i = 0; i < playerData.size(); i++) {
				
				Player tempPlayer = new Player("assets/cars/car" + (i+1) + ".png",
						636 + offset, 12018);
				
				// Place data in hashtable
				Double[] tempcoord = {(636.0+offset), 12018.0, 0.0};
				playerCoords.put(i+1, tempcoord);
				
				// Set player port and IP
				tempPlayer.setIpAddress(playerData.get(i).getAddress());
				tempPlayer.setPort(playerData.get(i).getPort());
				
				players.add(tempPlayer);

				offset = offset + 100;
			}
			
			System.out.println("ServerID: " + myID + "\nNumber of Players: " + players.size());
			
			break;
		
		case 2:
			
			try {
				socket = new DatagramSocket(clientPort);
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				log.println("Client socket creation", e.toString());
				System.exit(1);
			}

			Client client = new Client();
			myID = client.connectToServer();
			
			int numOfPlayers = client.getNumOfPlayers();
			
			for (int i = 0; i < numOfPlayers; i++) {
				Player tempPlayer = new Player("assets/cars/car" + i + ".png",
						636 + offset, 12018);
				
				// Place data in hashtable
				Double[] tempcoord = {(636.0+offset), 12018.0, 0.0};
				playerCoords.put(i, tempcoord);
				
				// Set server's IP and port
				if (i == 0) {
					
					// Set player port and IP
					try {
						tempPlayer.setIpAddress(InetAddress.getByName(serverIP));
						tempPlayer.setPort(serverPort);
					} catch (UnknownHostException e) {
						log.println("IP Error", "Client unable to get server IP & Port");
					}
				}
				
				players.add(tempPlayer);
				offset = offset + 100;
			}
			
			break;
			
		default:
			break;
		}

       	
		Thread sendThread = new Thread(new SendPacketThread());
		Thread receiveThread = new Thread(new ReceivePacketThread());
		
		sendThread.start();
		receiveThread.start();
		
        world = new World();
    }


    /** Update the game state for a frame.
     * @param gc The Slick game container object.
     * @param delta Time passed since last frame (milliseconds).
     */

    public static void updateWorld(GameContainer gc, int delta) throws SlickException{
    	
   	 // Get data about the current input (keyboard state).
       Input input = gc.getInput();

       // Update the player's rotation and position based on key presses.
       ArrayList<Double> rotateDir = new ArrayList<Double>();
       ArrayList<Double> moveDir = new ArrayList<Double>();
       double move_dir = 0, rotate_dir = 0;


       if (input.isKeyDown(Input.KEY_DOWN) || input.isKeyDown(Input.KEY_S))
           move_dir -= 1;
       if (input.isKeyDown(Input.KEY_UP) || input.isKeyDown(Input.KEY_W))
           move_dir += 1;
       if (input.isKeyDown(Input.KEY_LEFT) || input.isKeyDown(Input.KEY_A))
           rotate_dir -= 1;
       if (input.isKeyDown(Input.KEY_RIGHT) || input.isKeyDown(Input.KEY_D))

           rotate_dir += 1;
               
               rotateDir.add(rotate_dir);
               moveDir.add(move_dir);

              // rotate_dir = move_dir = 0;
               
//               if (input.isKeyDown(Input.KEY_S))
//           move_dir -= 1;
//       if (input.isKeyDown(Input.KEY_W))
//           move_dir += 1;
//       if ( input.isKeyDown(Input.KEY_A))
//           rotate_dir -= 1;
//       if ( input.isKeyDown(Input.KEY_D))
//           rotate_dir += 1;
//               
//               rotateDir.add(rotate_dir);
//               moveDir.add(move_dir);
//               rotate_dir = move_dir = 0;
       
       // Let World.update decide what to do with this data.
       world.update(rotate_dir, move_dir, delta, players, playerCoords);
       rotateDir = null;
       moveDir = null;
   }
    
    /** Render the entire screen, so it reflects the current game state.
     * @param gc The Slick game container object.
     * @param g The Slick graphics object, used for drawing.
     */
    public static void renderGame(Graphics g) throws SlickException {
        // Let World.render handle the rendering.
        world.render(g, players, playerCoords);
    }


    /** Start-up method. Creates the game and runs it.
     * @param args Command-line arguments (ignored).
     */
    public static void main(String[] args) throws SlickException
    {
    	AppGameContainer gc = new AppGameContainer(new Menu(), 800, 600, false);
    	//AppGameContainer app = new AppGameContainer(new Game());
        // setShowFPS(true), to show frames-per-second.
        gc.setShowFPS(false);
        gc.setDisplayMode(SCREENWIDTH, SCREENHEIGHT, false);
        gc.start();
    }

	@Override
	public void render(GameContainer arg0, Graphics arg1) throws SlickException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(GameContainer arg0, int arg1) throws SlickException {
		// TODO Auto-generated method stub
		
	}

}
