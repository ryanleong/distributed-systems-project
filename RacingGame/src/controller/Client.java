package controller;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.SocketException;

public class Client {


	public Client() {

	}

	public int connectToServer() {
		int myID = -1;

		try {

			byte[] sendData;
			byte[] receiveData = new byte[1024];

			// Send Request

			sendData = "15sf35fe2d".getBytes();

			DatagramPacket sendPacket = new DatagramPacket(sendData,
					sendData.length, InetAddress.getByName(Game.serverIP), Game.serverPort);
			Game.socket.send(sendPacket);

			// Receive Acknowledgment
			DatagramPacket receivePacket = new DatagramPacket(receiveData,
					receiveData.length);
			Game.socket.receive(receivePacket);

			String data = new String(receivePacket.getData(),
					receivePacket.getOffset(), receivePacket.getLength(),
					"UTF-8");

			System.out.println("My ID: " + data);
			myID = Integer.parseInt(data);

		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return myID;
	}

	public int getNumOfPlayers() {
		int numOfPlayers = 0;
		try {
			byte[] receiveData = new byte[1024];

			// Receive Acknowledgment
			DatagramPacket receivePacket = new DatagramPacket(receiveData,
					receiveData.length);
			Game.socket.receive(receivePacket);

			String data = new String(receivePacket.getData(),
					receivePacket.getOffset(), receivePacket.getLength(),
					"UTF-8");

			System.out.println("Number of Players: " + data);
			numOfPlayers = Integer.parseInt(data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return numOfPlayers;
	}
}
