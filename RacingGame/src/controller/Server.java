package controller;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.ArrayList;

public class Server {
	private int maxPlayers;
	private ArrayList<DatagramPacket> playerData;

	public Server(int maxPlayers) {
		this.maxPlayers = maxPlayers - 1;
		this.playerData = new ArrayList<DatagramPacket>();
	}

	public ArrayList<DatagramPacket> waitForClient() {

		try {

			byte[] receiveData = new byte[1024];

			for (int i = 0; i < maxPlayers; i++) {
				// Receive
				DatagramPacket receivePacket = new DatagramPacket(receiveData, receiveData.length);

				System.out.println("Waiting for connections...");
				// Waiting to receive
				Game.socket.receive(receivePacket);

				String data = new String(receivePacket.getData(), 0, receivePacket.getLength(), "UTF-8");
				System.out.println("Request Received from " + receivePacket.getAddress());

				// Check for unique string to ensure it is a connection request
				if (data.equals("15sf35fe2d")) {

					// Store Address and port of player
					playerData.add(receivePacket);

					// Spawn thread to add client to lobby
					Thread connection = new Thread(new ConnectClient(receivePacket, playerData.size()));
					connection.start();
				}
			}

		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Sleep to prevent packet out of order
		try {
			Thread.sleep(200);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// Send back number of players when all players are connected
		sendNumOfPlayers();

		return playerData;
	}

	private void sendNumOfPlayers() {

		for (DatagramPacket data : playerData) {
			byte[] sendData;

			InetAddress IPAddress = data.getAddress();
			int port = data.getPort();

			sendData = ("" + (playerData.size() + 1)).getBytes();

			DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);

			try {
				Game.socket.send(sendPacket);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}

// /////////////////////////////////////////////////////
/* Thread to connect clients to server */
// /////////////////////////////////////////////////////
class ConnectClient implements Runnable {

	private DatagramPacket receivePacket;
	private int id;
	private byte[] sendData;

	public ConnectClient(DatagramPacket received, int id) {
		// TODO Auto-generated constructor stub
		this.receivePacket = received;
		this.id = id;
	}

	@Override
	public void run() {

		InetAddress IPAddress = receivePacket.getAddress();
		int port = receivePacket.getPort();

		sendData = ("" + id).getBytes();

		DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, port);

		try {
			Game.socket.send(sendPacket);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
