package interfaces;

import models.Angle;
import models.Player;

import org.newdawn.slick.SlickException;

public interface PlayerInterface {
	
	/* 
	 * @param Angle - Angular Measure Object
	 */
	public abstract void render(Angle theta) throws SlickException;
	
	/* Compute the half x-value of the screen 
	 * @return The half x
	 */
	public int tileAlignX();

	/* Compute the half y-value of the screen 
	 * @return The half y
	 */
	public int tileAlignY();

	/* Updates the state of the player every frame 
	 * @param 
	 */
	public void update(double rotateDir, double moveDir, int delta,Angle theta,Player p1,int id,double mu);
	/* The below eight statements created getters and setters as
	 * the variables they are are private. This allows them to be 
	 * accessible in the World
	 */
	
	public double getPrevY();
	public void setPrevY(double prevY);
	public double getX();
	public void setX(double x);
	public double getY();
	public void setY(double y);
	public double getPrevX();
	
	
	
}
