Setting up
=============

Dependencies for LWJGL differ for different OS.
Instructions to set up will be done soon.

In the mean time, command line can be used to compile and run.


Compiling in Command Line
==========================
javac -cp lib/slick.jar:lib/lwjgl.jar:lib/tinysui.jar -d bin src/*/*.java


Running in Command Line
=========================
java -cp lib/slick.jar:lib/lwjgl.jar:lib/tinysui.jar:bin -Djava.library.path=lib/natives/linux controller.Game

For the library.path, replace with the OS you are running on.


Playing the Game
=========================
At least 2 instances of the Game needs to be running. One will act as a Server, the others will
act as Clients.

Currently:
Selection to be Server or Client is done in the Command Line Interface.


Sending/Receiving Packets
=========================
Packets are exchanged in the Game.java using UDP.
